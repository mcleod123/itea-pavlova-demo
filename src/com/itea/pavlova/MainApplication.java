package com.itea.pavlova;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class MainApplication {

    public static void main(String[] args) throws IOException {
        String string = "s-t-r";
        String[] values = string.split("-");
        System.out.println(values[10]);
        int valuesLength = string.split("-")
                .length;
    }

    private static int calculateSum(int... values) {
        int sum = 0;
        for (int value : values) {
            sum += value;
        }
        return sum;
    }

    private static int forTest() {
        System.out.println("In forTEst");
        throw new IllegalArgumentException();
    }

    private static boolean test(boolean flag) {
        return flag;
    }

    private static String testSwitchCase(String dayOfWeek) {
        switch (dayOfWeek) {
            case "Monday":
            case "Tuesday":
            case "Wednesday":
            case "Thursday":
            case "Friday":
                return "WORK!WORK!WORK!";
            case "Saturday":
            case "Sunday":
                return "Hey! Let's go to the bar!";
            default:
                return "I dunno";
        }
    }

    private static String testIfElse(String name) {
        if (name.length() == 0) {
            return "Such a strange name... It's blank";
        } else if (name.length() < 10) {
            return "Hey, nice to meet you, " + name;
        } else {
            return "Wow, I cannot pronounce this...";
        }
    }

    /**
     * Java Doc comment
     */
    @SuppressWarnings("NonAsciiCharacters")
    private static void demoDifferentTypesOfIntegers() {
//        int tenth = 22; //x - for tenth
//        int e = 022;
//        int z = 0x22;
//
//        System.out.println("Tenth: " + tenth);
//        System.out.println("Eighth: " + eighth);
//        System.out.println("Sixteenth: " + sixteenth);
        int testByte = 100;
        testByte += 5;
        short testShort = 324;
        System.out.println("Byte: " + testByte);
        System.out.println("Short: " + testShort);
        testByte = (byte) testShort;
        System.out.println("byte after conversion: " + testByte);

        int testUnary = 8;
        testUnary++;
        ++testUnary;

    }

    private static void testUnary(int output) {
        System.out.println("After operation: " + output);
    }

    private void test() {
        int input = 5;
        if (input > 0) {
            System.out.print(input);
        }
        if (input < 10) {
            System.out.print(input);
        }
    }

    private boolean checkTest() {
        throw new IllegalArgumentException();
    }
}