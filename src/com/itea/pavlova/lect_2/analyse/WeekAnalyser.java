package com.itea.pavlova.lect_2.analyse;

public class WeekAnalyser {

    public String analyseDayOfWeekAction(String dayOfWeek) {
        return switch (dayOfWeek) {
            case "Monday", "Tuesday" -> "I. Don't. Want. To. Wooooooooooooooork";
            case "Wednesday" -> "Eww, we're in the middle";
            case "Thursday" -> "Thursday is a small Friday";
            case "Friday", "Saturday", "Sunday" -> "WHY SO FAST?";
            default -> "Hey, perform your inputs carefully";
        };
    }

    public String analyseDayOfWeekDifficulty(String dayOfWeek) {
        if (dayOfWeek.length() == 0) {
            return "It's too small, isn't it?";
        } else if (dayOfWeek.length() == 6 && dayOfWeek.startsWith("S")) {
            return "It's Sunday. Just chill";
        } else if(dayOfWeek.startsWith("S")) {
            return "Saturday does not exist. It's just a sleepy day";
        } if (dayOfWeek.length() > 6) {
            return "It's a difficult working day...";
        } else {
            return "It's just the first/last day of working week.";
        }
    }
}
