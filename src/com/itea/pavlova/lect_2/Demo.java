package com.itea.pavlova.lect_2;

import com.itea.pavlova.lect_2.analyse.WeekAnalyser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Demo {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        WeekAnalyser analyser = new WeekAnalyser();
        System.out.print("Input number of iterations: ");
        int numberOfIterations = Integer.parseInt(reader.readLine());
        int counter = 0;
        do {
            System.out.print("Input your day of the week: ");
            String dayOfWeek = reader.readLine();
            System.out.println("Action: " + analyser.analyseDayOfWeekAction(dayOfWeek));
            System.out.println("Difficulty: " + analyser.analyseDayOfWeekDifficulty(dayOfWeek));
            if (numberOfIterations - (++counter) == 1) {
                System.out.println("Hey it's your last try");
            }
        } while (counter < numberOfIterations);
    }
}
