package com.itea.pavlova.lect_4;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Objects;

import static java.util.Objects.*;

public class Demo {

    public static void main(String[] args) {
        ShoppingList shoppingList = new ShoppingList();
        shoppingList.addShoppingList("milk");
        shoppingList.addShoppingList("coffee");
        shoppingList.viewShoppingList();
        shoppingList.removeFromShoppingList(1);
        shoppingList.viewShoppingList();
        shoppingList.addShoppingList("butter");
        shoppingList.addShoppingList("cinnabons");
        shoppingList.addShoppingList("beer");
        shoppingList.viewShoppingList();
        shoppingList.removeFromShoppingList("milk");
        shoppingList.viewShoppingList();
    }

    private void modifyArray(int[] incomeArray) {
        for (int i = 0; i < incomeArray.length; i++) {
            if (incomeArray[i] == 0 || incomeArray[i] % 2 == 0) {
                incomeArray[i] = 21;
            }
        }
    }

    private void viewArray(int[][] incomeArray) {
        for (int i = 0; i < incomeArray.length; i++) {
            for (int j = 0; j < incomeArray[i].length; j++) {
                System.out.println("Value: " + incomeArray[i][j]);
            }
        }
    }

    private int calculateSum(int... incomeValues) {
        int sum = 0;
        for (int value : incomeValues) {
            sum += value;
        }
        return sum;
    }

    private void viewArray(int[] incomeArray) {
        System.out.println("Array size: " + incomeArray.length);

        for (int value : incomeArray) {
            System.out.println("Value: " + value);
        }
    }

    private void viewArray(String[] incomeArray) {
        System.out.println("Array size: " + incomeArray.length);
        for (String value : incomeArray) {
            if (isNull(value)) {
                System.out.println("You value is null!");
            } else if (value.isEmpty()) {
                System.out.println("Empty string");
            } else {
                System.out.println("Your modified (maybe) value: " + value.replace('n', 'u'));
            }
        }
    }

}
