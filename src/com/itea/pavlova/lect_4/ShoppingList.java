package com.itea.pavlova.lect_4;

import java.util.Arrays;

public class ShoppingList {

    private static final int DEFAULT_LIST_CAPACITY = 10;

    private String[] shoppingItems;
    private int actualSize;

    public ShoppingList() {
        shoppingItems = new String[DEFAULT_LIST_CAPACITY];
    }

    public void addShoppingList(String shoppingListItem) {
        if (actualSize >= shoppingItems.length - 1) {
            shoppingItems = Arrays.copyOf(shoppingItems, shoppingItems.length * 2 / 3);
        }
        shoppingItems[actualSize++] = shoppingListItem;
        System.out.println("New item '" + shoppingListItem + "' was added");
    }

    public boolean removeFromShoppingList(int index) {
        if (index > 0 && index <= actualSize - 1) {
            System.arraycopy(shoppingItems, index + 1, shoppingItems, index, shoppingItems.length - index - 1);
            actualSize--;
            return true;
        }
        System.out.println("This index cannot be used: " + index);
        return false;
    }

    public boolean removeFromShoppingList(String shoppingItem) {
        int index = findIndexOfShoppingItem(shoppingItem);
        if (index < 0) {
            System.out.println("This shopping item does not exist: " + shoppingItem);
            return false;
        }
        return removeFromShoppingList(index);
    }

    public void viewShoppingList() {
        System.out.println("---------Your shopping list");
        for (int i = 0; i < actualSize; i++) {
            if (shoppingItems[i].contains("beer")) {
                System.out.println("We won't buy beer. You have to take care of your health!");
            } else if (shoppingItems[i].startsWith("choco")) {
                System.out.println("You'd better go to gym");
            } else {
                System.out.println((i + 1) + ". " + shoppingItems[i]);
            }
        }
    }

    private int findIndexOfShoppingItem(String shoppingItem) {
        for (int i = 0; i < actualSize; i++) {
            if (shoppingItem.equals(shoppingItems[i])) {
                return i;
            }
        }
        return -1;
    }
}
