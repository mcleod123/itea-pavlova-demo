package com.itea.pavlova.lect_3;

public class CastleBuilder {

    private Castle castle;

    public CastleBuilder buildGarden(int size) {
        castle.setGardenSize(size);
        return this;
    }

    public CastleBuilder buildPool(boolean hasPool) {
        castle.setHasPool(hasPool);
        return this;
    }

    public CastleBuilder buildInCity(boolean isInCity) {
        castle.setInCity(isInCity);
        return this;
    }

    public Castle build() {
        return castle;
    }

    public Castle buildDefaultCastle() {
        return this.buildGarden(10)
                .buildInCity(true)
                .buildPool(false)
                .build();
    }
}
