package com.itea.pavlova.lect_3;

public class Castle {

    private int windowsNumber;
    private boolean isInCity;
    private boolean hasPool;
    private int gardenSize;

    public Castle(int windowsNumber, boolean isInCity, boolean hasPool, int gardenSize) {
        this.windowsNumber = windowsNumber;
        this.isInCity = isInCity;
        this.hasPool = hasPool;
        this.gardenSize = gardenSize;
    }

    public void setWindowsNumber(int windowsNumber) {
        this.windowsNumber = windowsNumber;
    }

    public void setInCity(boolean inCity) {
        isInCity = inCity;
    }

    public void setHasPool(boolean hasPool) {
        this.hasPool = hasPool;
    }

    public void setGardenSize(int gardenSize) {
        this.gardenSize = gardenSize;
    }
}
