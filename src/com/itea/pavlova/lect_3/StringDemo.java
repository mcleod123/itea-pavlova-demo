package com.itea.pavlova.lect_3;

import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringDemo {
    public static void main(String[] args) {

//        Castle castle = new CastleBuilder()
//                .buildDefaultCastle();
//
//        StringDemo stringDemo = new StringDemo();
//        StringBuilder stringBuilder = new StringBuilder();
//        StringBuffer stringBuffer = new StringBuffer();
//        String test = "dsd" + "sdsd" + "sdsd";
//        String output = stringBuffer.append("")
//                .append("")
//                .toString();
//        stringBuffer.append();
//
//        stringBuilder
//                .append("first")
//                .append(", ")
//                .append("sth")
//                .append(", ")
//                .append(1)
//                .toString();
    }

    private void demoStringHeap() {
        viewDemoLine("String Heap");
        String apple = "Apple"; // String Pool: 1. Searching for "Apple" 2. +1
        String anotherApple = "Apple";// +1 Heap
        String extraApple = "Apple"; // String Poll: 1. Searching for "Apple"
        System.out.println("In heap: " + (apple == extraApple));
        System.out.println("Not in heap: " + (anotherApple == apple));
        System.out.println("It has to be in heap now: " + (anotherApple.intern() == apple));
    }

    private void concatStrings() {
        viewDemoLine("Concat Strings");
        String apple = "apple";
        System.out.println("red ".concat(apple));
        System.out.println("red " + apple);
    }

    private void replaceStrings() {
        viewDemoLine("Replace Strings");
        String apple = "123ppppppppppppppppppppp456";
        System.out.println(apple.replaceFirst("p{5}", "peach "));
        System.out.println(apple.replaceAll("p{5}", "peach "));
    }

    private void repeatStrings() {
        viewDemoLine("Repeat Strings");
        String apple = "apple";
        System.out.println(apple.repeat(5));
    }

    private void demoRegexSearch() {
        viewDemoLine("Regex Search");
        String apple = "red 12";
        Pattern pattern = Pattern.compile("pl{2,4}");
        Matcher matcher = pattern.matcher(apple);
        System.out.println(matcher.find());
    }

    private void demoSplit() {
        viewDemoLine("Split");
        String shoppingList = "apples, pears, peaches, milk, tea";
        String[] shoppingPositions = shoppingList.split(", ");

        for (int i = 0; i < shoppingPositions.length; i++) {
            System.out.println((i + 1) + ". " + shoppingPositions[i]);
        }

        Arrays.asList(shoppingPositions)
                .forEach((position) -> System.out.println("Using stream " + position));

        for (int i = 0; i < shoppingPositions.length; i++) {
            if (shoppingPositions[i].endsWith("s")) {
                System.out.println("We have found it: " + shoppingPositions[i]);
            }

        }
    }

    private void demoTokenizer() {
        viewDemoLine("String Tokenizer");
        int count = 0;
        String shoppingList = "apples, pears, peaches, milk, tea";
        StringTokenizer stringTokenizer = new StringTokenizer(shoppingList, ", ");

        while (stringTokenizer.hasMoreElements()) {
            System.out.println(++count + ". " + stringTokenizer.nextElement());
        }
        stringTokenizer = new StringTokenizer(shoppingList, ", ");
        System.out.println("Is there are any more elements?" + stringTokenizer.hasMoreElements());
        System.out.println(stringTokenizer.nextElement());
    }

    private void viewDemoLine(String name) {
        System.out.println("=======================Demo " + name);
    }
}
