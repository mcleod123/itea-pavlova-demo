package com.itea.pavlova.lect_3;

import java.util.Arrays;

public final class ImmutableObject {

    private int smth;
    private final String extraSmth;

    public ImmutableObject(int smth, String extraSmth) {
        this.smth = smth;
        this.extraSmth = extraSmth;
    }

    public String getExtraSmth() {
        return Arrays.toString(extraSmth.getBytes().clone());
    }
}
