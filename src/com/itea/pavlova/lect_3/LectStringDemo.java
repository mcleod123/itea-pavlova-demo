package com.itea.pavlova.lect_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LectStringDemo {

    public static void main(String[] args) throws IOException {
        LectStringDemo stringDemo = new LectStringDemo();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        StringBuilder shoppingList = new StringBuilder();
        System.out.println("Hi! We expect your shopping list");
        stringDemo.collectShoppingList(shoppingList, reader);
        stringDemo.viewCorrectShoppingList(shoppingList);
        System.out.println("Do you want something extra to add? (Y/N)");
        if (reader.readLine().equals("Y")) {
            stringDemo.collectShoppingList(shoppingList, reader);
            stringDemo.viewCorrectShoppingList(shoppingList);
        } else {
            System.out.println("Thanks for you shopping list. Bye!");
        }
    }

    private void collectShoppingList(StringBuilder shoppingList, BufferedReader reader) throws IOException {
        System.out.println("If you have nothing to add then type \"That's enough\"");
        boolean isEnough = false;
        while (!isEnough) {
            System.out.print("Input your shopping item: ");
            String shoppingItem = reader.readLine();
            if (shoppingItem.equals("That's enough")) {
                isEnough = true;
            } else {
                if (shoppingList.length() == 0) {
                    shoppingList.append(shoppingItem);
                } else {
                    shoppingList.append(", ")
                            .append(shoppingItem);
                }
            }
        }
    }

    private void viewCorrectShoppingList(StringBuilder shoppingList) {
        String shoppingListString = shoppingList.toString();
        System.out.println("That's your shopping list: ");
        String[] shoppingListItems = shoppingListString.split(", ");
        for (int i = 0; i < shoppingListItems.length; i++) {
            if (shoppingListItems[i].contains("beer")) {
                System.out.println("We won't buy beer. You have to take care of your health!");
            } else if (shoppingListItems[i].startsWith("choco")) {
                System.out.println("You'd better go to gym");
            } else {
                System.out.println((i + 1) + ". " + shoppingListItems[i]);
            }
        }
    }
}
