package com.itea.pavlova.lect_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;


public class Demo_1 {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> listOfFood = new ArrayList<>();
        System.out.println("=======Welcome======");
        startMenuList(reader, listOfFood);
    }

    public static void startMenuList(BufferedReader reader, ArrayList<String> listOfFood) throws IOException {
        System.out.println("\n"+"Make your choice: ");
        System.out.println("View product list - 1");
        System.out.println("Add new product - 2");
        System.out.println("Delete some product - 3");
        System.out.println("Exit - 4");
        System.out.print("Enter: ");
        String userChooseInStartMenu = reader.readLine();
        whenUserMadeChoose(userChooseInStartMenu, listOfFood, reader);
    }

    public  static void whenUserMadeChoose(String userChooseInStartMenu, ArrayList<String> listOfFood, BufferedReader reader ) throws IOException {
        switch (userChooseInStartMenu) {
            case "1" -> ifUserInputOne(listOfFood, reader);
            case "2" -> ifUserSelectTwo(listOfFood, reader);
            case "3" -> ifUserSelectThree(listOfFood, reader);
            case "4" -> ifUserSelectFour(listOfFood, reader);
            default -> {
                System.out.println("Not correct");
                startMenuList(reader, listOfFood);
            }
        }
    }

    public static void ifUserInputOne(ArrayList<String> listOfFood, BufferedReader reader)throws IOException{
        if(!listOfFood.isEmpty()) {
            for (int i = 0; i < listOfFood.size(); i++) {
                System.out.println((i + 1) + ". " + listOfFood.get(i));
            }
        }
        else{
            System.out.println("Your list is empty");
            ifUserSelectTwo(listOfFood, reader);
        }
        startMenuList(reader, listOfFood);
    }

    public static void ifUserSelectTwo(ArrayList<String> listOfFood, BufferedReader reader) throws IOException {
        System.out.println("\n"+"Add new product");
        System.out.println("If you want to exit to the menu enter \"1\" ");
        String newProductInList =  brandNameInFood(reader);

        while (!newProductInList.equals("1")) {
            listOfFood.add(newProductInList);
            System.out.println("OK!");
            newProductInList = brandNameInFood(reader);
        }
        startMenuList(reader, listOfFood);
    }

    public static void ifUserSelectThree(ArrayList<String> listOfFood, BufferedReader reader) throws IOException {
        for (int i = 0; i < listOfFood.size(); i++) {
            System.out.println((i + 1) + ". " + listOfFood.get(i));
        }
        System.out.print("\n"+"Enter number element of food want you delete: ");
        try {
            String numberForDeleteFoodFromTheList = reader.readLine();
            int convertStringToInt = Integer.parseInt(numberForDeleteFoodFromTheList)-1;
            listOfFood.remove(convertStringToInt);
            ifUserInputOne(listOfFood,reader);
        }
        catch (NumberFormatException ex){
            ex.printStackTrace();
            System.out.println("Only numbers");
            startMenuList(reader, listOfFood);
        }
    }

    public static void ifUserSelectFour(ArrayList<String> listOfFood, BufferedReader reader ) throws IOException {
        System.out.println("\n"+"Enter: \"That`s all\" for Exit");
        String fieldForExit = reader.readLine().trim().toLowerCase(Locale.ROOT);
        if (fieldForExit.trim().equals("that`s all") || fieldForExit.equals("thats all")){
            System.out.println("Goodbye!");
        }
        else {startMenuList(reader, listOfFood);}
    }

    public static String brandNameInFood(BufferedReader reader) throws IOException {
        String stringForChangesWithBrand = reader.readLine().trim().toLowerCase(Locale.ROOT);

        if (stringForChangesWithBrand.contains("roshen")){
            stringForChangesWithBrand = stringForChangesWithBrand.replace("roshen", "Sweetness");
        }
        else if(stringForChangesWithBrand.contains("beer")){
            System.out.println("Not today,bro");
            stringForChangesWithBrand = "Carrot";
        }
        return productsWithEndingSOREs(stringForChangesWithBrand, reader);

    }

    public static String productsWithEndingSOREs(String stringForReturnManyFoods, BufferedReader reader) throws IOException {
        if(stringForReturnManyFoods.endsWith("es") || stringForReturnManyFoods.endsWith("s")){
            stringForReturnManyFoods = forAmountOfFood(stringForReturnManyFoods, reader);
        }
        return stringForReturnManyFoods;
    }

    public static String forAmountOfFood(String stringForReturnManyFoods, BufferedReader reader) throws IOException {
        System.out.print("How mach? : ");
        String amountFoods = reader.readLine();
        return stringForReturnManyFoods + " amount: " + amountFoods;
    }
}
